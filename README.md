- Unit 0x00 - Getting started with C++
- Unit 0x01 - Basic structures 
- Unit 0x02 - References, structs, auto, exceptions
- Unit 0x03 - Class, ctor, dtor, member
- Unit 0x04 - Templates (Generics)

- Mock Exam + Discussion

- Unit 0x05 - Asm, Pointers
- Unit 0x06 - Dynamic memory allocation (Heap), new, delete, const
- Unit 0x07 - Operations, friend
- Unit 0x08 - Inheritance
- Unit 0x09 - template specialisation, smart ptr.

- Mock Exam + Discussion

- Unit 0x0a - Lambdas, Algorithms, Code Organisation
- Unit 0x0b - Threads, Mutex, Futures
- Unit 0x0c - Moving + 'Golden Words'
